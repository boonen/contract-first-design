package nl.janboonen.oas.contractfirstdesign.cat;

import nl.janboonen.oas.contractfirstdesign.cat.application.FindAssistantCatUseCase;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = FindAssistantCatUseCase.class)
public class TestAclConfiguration {
}
