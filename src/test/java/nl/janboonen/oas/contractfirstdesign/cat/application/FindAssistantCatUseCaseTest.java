package nl.janboonen.oas.contractfirstdesign.cat.application;

import nl.janboonen.oas.contractfirstdesign.cat.AntiCorruptionLayerTest;
import nl.janboonen.oas.contractfirstdesign.events.CatAssistantFoundEvent;
import nl.janboonen.oas.contractfirstdesign.events.RequestCatAssistantCommand;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.HttpError;
import org.mockserver.model.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.modulith.test.ApplicationModuleTest;
import org.springframework.modulith.test.PublishedEvents;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Testcontainers
@AntiCorruptionLayerTest
@ApplicationModuleTest
class FindAssistantCatUseCaseTest {

    @Autowired
    private FindAssistantCatUseCase useCase;

    public static final DockerImageName MOCKSERVER_IMAGE = DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion());

    @Container
    public static final MockServerContainer mockServer = new MockServerContainer(MOCKSERVER_IMAGE)
            .withEnv("MOCKSERVER_LOG_LEVEL", "DEBUG");

    private static MockServerClient mockServerClient;

    // Dynamic property to inject the container's URL into the Spring context
    @DynamicPropertySource
    static void mockServerProperties(DynamicPropertyRegistry registry) {
        // Testcontainers does not play nice with Spring Modulith's ApplicationModuleTest, so we need to manually start the container
        if (!mockServer.isRunning()) {
            mockServer.start();
        }
        registry.add("contract-first-design.cat.base-url", () -> "http://" + mockServer.getHost() + ":" + mockServer.getMappedPort(1080));
        registry.add("contract-first-design.cat.connection-timeout", () -> 1000);
    }

    @BeforeAll
    static void beforeAll() {
        mockServerClient = new MockServerClient(mockServer.getHost(), mockServer.getMappedPort(1080));
    }

    @BeforeEach
    void setUp() {
        mockServerClient.reset();
    }

    @Test
    void givenValidCatFactApiRequest_whenAssistantRequested_thenPublishEvent(PublishedEvents events) {
        // Given
        mockServerClient.when(
                        request()
                                .withMethod("GET")
                                .withPath("/breeds")
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withContentType(MediaType.APPLICATION_JSON)
                                .withBody("{ \"breeds\": [\"Bengal\", \"Siamese\", \"Persian\"] }")
                );
        final String givenBeneficiary = "Mr Grumpy";
        final RequestCatAssistantCommand givenCommand = buildRequestCatAssistantCommand(givenBeneficiary);

        // When
        useCase.on(givenCommand);

        // Then
        events.ofType(CatAssistantFoundEvent.class)
                .matching(CatAssistantFoundEvent::getBreed, "Bengal")
                .matching(CatAssistantFoundEvent::getBeneficiary, givenBeneficiary);
    }

    private static RequestCatAssistantCommand buildRequestCatAssistantCommand(String givenBeneficiary) {
        return new RequestCatAssistantCommand()
                .requester(givenBeneficiary)
                .duty(RequestCatAssistantCommand.DutyEnum.EMPLOYEE_WELL_BEING)
                .preferredBreed("test");
    }

    @Test
    void givenHttp500ResponseOnValidCatFactApiRequest_whenAssistantRequested_thenNoEventIsPublished(PublishedEvents events) {
        // Given
        // Mock an HTTP 500 server error response
        mockServerClient.when(
                        request()
                                .withMethod("GET")
                                .withPath("/breeds"))
                .respond(
                        response()
                                .withStatusCode(500)
                                .withBody("{ \"error\": \"Internal Server Error\" }"));

        final String givenBeneficiary = "Mr Grumpy";
        final RequestCatAssistantCommand givenCommand = buildRequestCatAssistantCommand(givenBeneficiary);

        // When
        useCase.on(givenCommand);

        // Then
        assertThat(events.eventOfTypeWasPublished(CatAssistantFoundEvent.class)).isFalse();
    }

    @Test
    void givenConnectionTimeoutOnValidCatFactApiRequest_whenAssistantRequested_thenNoEventIsPublished(PublishedEvents events) {
        // Given
        // Simulate a connection timeout
        mockServerClient.when(
                        request()
                                .withMethod("GET")
                                .withPath("/breeds"))
                .respond(
                        response()
                                .withStatusCode(200)
                                .withContentType(MediaType.APPLICATION_JSON)
                                .withBody("{ \"breeds\": [\"Bengal\", \"Siamese\", \"Persian\"] }")
                                .withDelay(TimeUnit.SECONDS, 2));

        final String givenBeneficiary = "Mr Grumpy";
        final RequestCatAssistantCommand givenCommand = buildRequestCatAssistantCommand(givenBeneficiary);

        // When
        useCase.on(givenCommand);

        // Then
        assertThat(events.eventOfTypeWasPublished(CatAssistantFoundEvent.class)).isFalse();
    }

}