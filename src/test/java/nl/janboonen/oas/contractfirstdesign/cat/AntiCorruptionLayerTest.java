package nl.janboonen.oas.contractfirstdesign.cat;

import nl.janboonen.oas.contractfirstdesign.cat.config.ApiConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@SpringJUnitConfig
@ContextConfiguration(classes = {ApiConfiguration.class, TestAclConfiguration.class})
@TestPropertySource(locations = {"classpath:application.yml", "classpath:application-test.yml"})
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface AntiCorruptionLayerTest {
}

