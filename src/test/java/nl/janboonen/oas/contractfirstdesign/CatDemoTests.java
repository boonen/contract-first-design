package nl.janboonen.oas.contractfirstdesign;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.modulith.core.ApplicationModules;

@SpringBootTest
class CatDemoTests {

    @Test
    void verifyModularStructure() {
        ApplicationModules.of(CatDemo.class).verify();
    }

}
