package nl.janboonen.oas.contractfirstdesign.assistant.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Assistant {

    @JsonIgnore
    private UUID id;

    private String name;

    private String duty;

    private String beneficiary;

}
