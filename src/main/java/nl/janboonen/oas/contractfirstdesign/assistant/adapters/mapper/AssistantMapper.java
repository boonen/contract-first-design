package nl.janboonen.oas.contractfirstdesign.assistant.adapters.mapper;

import nl.janboonen.oas.contractfirstdesign.assistant.adapter.controller.dto.AssistantModel;
import nl.janboonen.oas.contractfirstdesign.assistant.adapter.controller.dto.AssistantRequestModel;
import nl.janboonen.oas.contractfirstdesign.assistant.adapter.controller.dto.AssistantSummaryModel;
import nl.janboonen.oas.contractfirstdesign.assistant.domain.Assistant;
import nl.janboonen.oas.contractfirstdesign.events.CatAssistantFoundEvent;
import nl.janboonen.oas.contractfirstdesign.events.RequestCatAssistantCommand;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface AssistantMapper {

    AssistantSummaryModel toAssistantSummaryModel(Assistant assistant);

    List<AssistantSummaryModel> toAssistSummaryModelList(List<Assistant> assistants);

    Assistant fromEvent(CatAssistantFoundEvent event);

    AssistantModel toAssistantModel(Assistant assistant);

    @Mapping(source = "beneficiary", target = "requester")
    RequestCatAssistantCommand fromCommand(AssistantRequestModel assistantRequestModel);

    String fromDutyEnum(AssistantRequestModel.DutyEnum duty);

}
