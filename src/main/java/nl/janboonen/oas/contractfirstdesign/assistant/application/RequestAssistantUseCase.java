package nl.janboonen.oas.contractfirstdesign.assistant.application;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.oas.contractfirstdesign.assistant.adapters.mapper.AssistantMapper;
import nl.janboonen.oas.contractfirstdesign.assistant.domain.Assistant;
import nl.janboonen.oas.contractfirstdesign.assistant.infrastructure.AssistantManagement;
import nl.janboonen.oas.contractfirstdesign.events.CatAssistantFoundEvent;
import nl.janboonen.oas.contractfirstdesign.events.RequestCatAssistantCommand;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class RequestAssistantUseCase {

    private final ApplicationEventPublisher eventPublisher;

    private final AssistantMapper assistantMapper;

    private final AssistantManagement assistantManagement;

    public void send(RequestCatAssistantCommand command) {
        eventPublisher.publishEvent(command);
        log.info("Send request for a new Office Assistant for employee {}.", command.getRequester());
    }

    @ApplicationModuleListener
    public void fromEvent(CatAssistantFoundEvent event) {
        Assistant assistant = assistantMapper.fromEvent(event);
        assistantManagement.registerAssistant(assistant);
    }

}
