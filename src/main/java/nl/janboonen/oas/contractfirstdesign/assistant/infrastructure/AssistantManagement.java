package nl.janboonen.oas.contractfirstdesign.assistant.infrastructure;

import lombok.extern.slf4j.Slf4j;
import nl.janboonen.oas.contractfirstdesign.assistant.domain.Assistant;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
public class AssistantManagement {

    private final Map<UUID, Assistant> assistants = new HashMap<>();

    public List<Assistant> getAllAssistants() {
        return assistants.values().stream().toList();
    }

    public void registerAssistant(Assistant assistant) {
        assistants.put(assistant.getId(), assistant);
        log.info("Stored assistant {} serving employee {} using ID {}.", assistant.getName(), assistant.getBeneficiary(), assistant.getId());
    }

    public Assistant getAssistantById(String id) {
        UUID key = UUID.fromString(id);
        if(assistants.containsKey(key)) {
            return assistants.get(key);
        }
        return null;
    }
}
