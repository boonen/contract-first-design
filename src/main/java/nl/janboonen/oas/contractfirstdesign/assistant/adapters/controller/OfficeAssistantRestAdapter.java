package nl.janboonen.oas.contractfirstdesign.assistant.adapters.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.oas.contractfirstdesign.assistant.adapter.controller.AssistantApi;
import nl.janboonen.oas.contractfirstdesign.assistant.adapter.controller.dto.AssistantModel;
import nl.janboonen.oas.contractfirstdesign.assistant.adapter.controller.dto.AssistantRequestModel;
import nl.janboonen.oas.contractfirstdesign.assistant.adapter.controller.dto.AssistantSummaryModel;
import nl.janboonen.oas.contractfirstdesign.assistant.adapters.mapper.AssistantMapper;
import nl.janboonen.oas.contractfirstdesign.assistant.application.RequestAssistantUseCase;
import nl.janboonen.oas.contractfirstdesign.assistant.infrastructure.AssistantManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RestController
@RequestMapping("/v1")
@AllArgsConstructor
public class OfficeAssistantRestAdapter implements AssistantApi {

    private final AssistantManagement assistantManagement;

    private final RequestAssistantUseCase requestAssistantUseCase;

    private final AssistantMapper assistantMapper;

    @Override
    public CompletableFuture<List<AssistantSummaryModel>> getActiveAssistants() {
        return CompletableFuture.completedFuture(assistantMapper.toAssistSummaryModelList(assistantManagement.getAllAssistants()));
    }

    @Override
    public CompletableFuture<AssistantModel> getAssistantById(String id) {
        return CompletableFuture.completedFuture(assistantMapper.toAssistantModel(assistantManagement.getAssistantById(id)));
    }

    @Override
    public CompletableFuture<Void> requestOfficeAssistant(AssistantRequestModel assistantRequestModel) {
        return CompletableFuture.runAsync(() -> requestAssistantUseCase.send(assistantMapper.fromCommand(assistantRequestModel)));
    }
}
