package nl.janboonen.oas.contractfirstdesign.cat.application;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.oas.contractfirstdesign.cat.adapater.catfacts.dto.Breed;
import nl.janboonen.oas.contractfirstdesign.cat.adapater.catfacts.dto.GetBreeds200Response;
import nl.janboonen.oas.contractfirstdesign.cat.adapter.ApiException;
import nl.janboonen.oas.contractfirstdesign.cat.adapter.catfacts.BreedsApi;
import nl.janboonen.oas.contractfirstdesign.events.CatAssistantFoundEvent;
import nl.janboonen.oas.contractfirstdesign.events.RequestCatAssistantCommand;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.modulith.events.ApplicationModuleListener;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Slf4j
@Component
@AllArgsConstructor
public class FindAssistantCatUseCase {

    private final Faker faker = new Faker();

    private final BreedsApi breedsApi;

    private final ApplicationEventPublisher eventPublisher;

    @ApplicationModuleListener
    public void on(RequestCatAssistantCommand command) {
        log.info("Received request to find an office cat for {}.", command.getRequester());
        var matchingAssistant = findMatchingCat(command);
        if (matchingAssistant != null) {
            CatAssistantFoundEvent event = new CatAssistantFoundEvent()
                    .beneficiary(command.getRequester())
                    .breed(matchingAssistant.getBreed())
                    .name(faker.name().firstName());
            eventPublisher.publishEvent(event);
        } else {
            log.warn("Could not find a matching assistant for requester {}.", command.getRequester());
        }
    }

    private Breed findMatchingCat(RequestCatAssistantCommand command) {
        try {
            GetBreeds200Response breeds = breedsApi.getBreeds(1L);
            if (!CollectionUtils.isEmpty(breeds.getData())) {
                return breeds.getData().getFirst();
            }
        } catch (ApiException e) {
            log.warn("Could not retrieve Cat Breed from API. A default Cat Assistant is returned.", e);
        }
        return null;
    }

}
