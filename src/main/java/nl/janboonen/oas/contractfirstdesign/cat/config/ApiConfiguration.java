package nl.janboonen.oas.contractfirstdesign.cat.config;

import nl.janboonen.oas.contractfirstdesign.cat.adapter.ApiClient;
import nl.janboonen.oas.contractfirstdesign.cat.adapter.catfacts.BreedsApi;
import org.apache.hc.client5.http.config.ConnectionConfig;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;
import org.apache.hc.core5.util.Timeout;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ApiConfiguration.CatFactsProperties.class)
public class ApiConfiguration {

    @Bean
    public HttpClientConnectionManager connectionManager(CatFactsProperties properties) {
        PoolingHttpClientConnectionManager poolingConnManager = new PoolingHttpClientConnectionManager();
        poolingConnManager.setDefaultConnectionConfig(ConnectionConfig.custom()
                .setConnectTimeout(Timeout.ofMilliseconds(properties.connectionTimeout()))
                .setSocketTimeout(Timeout.ofMilliseconds(properties.connectionTimeout()))
                .build());
        return poolingConnManager;
    }

    @Bean
    public CloseableHttpClient httpClient(HttpClientConnectionManager connectionManager) {
        return HttpClients.custom()
                .useSystemProperties()
                .disableCookieManagement()
                .setConnectionManager(connectionManager)
                .build();
    }

    @Bean
    public ApiClient catApiClient(CloseableHttpClient httpClient, CatFactsProperties properties) {
        return new ApiClient()
                .setHttpClient(httpClient)
                .setConnectTimeout(properties.connectionTimeout())
                .setBasePath(properties.baseUrl);
    }

    @Bean
    public BreedsApi breedsApi(ApiClient apiClient) {
        return new BreedsApi(apiClient);
    }


    @ConfigurationProperties(prefix = "contract-first-design.cat")
    public record CatFactsProperties(String baseUrl, String apikey, int connectionTimeout) {
    }

}


