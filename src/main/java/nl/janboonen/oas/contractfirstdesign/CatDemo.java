package nl.janboonen.oas.contractfirstdesign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatDemo {

    public static void main(String[] args) {
        SpringApplication.run(CatDemo.class, args);
    }

}
