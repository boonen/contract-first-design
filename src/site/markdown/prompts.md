# ChatGPT Prompts

This pages gives an overview of all prompts that were used to generate code for this project. Parts of the code have
generated using [ChatGPT4](https://chat.openai.com).

### HTTP client

For demonstration, we use IntelliJ's HTTP Client. In the source file [office_api_client.http](files/office_api_client.http) you
can find requests that were generated using the following prompt:

```chatgpt
Given the uploaded OpenAPI specification I'd like you to generate HTTP requests in the format of IntelliJ's HTTP client. 

These are my requirements:
- Choose funny values for the variables instead of default names like 'string'. 
- Choose humorous values for request bodies.
- Give me the full set of requests in one code block.
- Use {{baseUrl}} as variable to prefix the server URL.
- Initialise the variable baseUrl with "http://localhost:8080".
- Use variables for path parameters.
```

**Note:** you must also upload the API spec to ChatGPT as context.